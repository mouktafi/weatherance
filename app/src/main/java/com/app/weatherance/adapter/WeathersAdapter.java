package com.app.weatherance.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.app.weatherance.R;
import com.app.weatherance.model.Weather;

import java.util.ArrayList;


public class WeathersAdapter extends ArrayAdapter<Weather> {
    private ArrayList<Weather> weathers;
    private LayoutInflater inflater;
    private int Resource;
    Context context;

    public WeathersAdapter(@NonNull Context context, int resource, ArrayList<Weather> objects) {
        super(context, resource, objects);

        this.context = context;
        this.Resource = resource;
        this.weathers = objects;
        inflater = (LayoutInflater.from(context));
    }

    private class ViewHolder {
        TextView name;
        TextView country;
        TextView temp;
        TextView desc;
        ImageView icon2;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(Resource, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView)convertView.findViewById(R.id.tv_name);
            viewHolder.country = (TextView)convertView.findViewById(R.id.tv_country);
            viewHolder.temp = (TextView)convertView.findViewById(R.id.tv_temp);
            viewHolder.desc = (TextView)convertView.findViewById(R.id.tv_description);
           // viewHolder.icon = (WebView)convertView.findViewById(R.id.weather_icon);
            viewHolder.icon2 = (ImageView)convertView.findViewById(R.id.imageView);
            //viewHolder.icon = (RequestCreator)convertView.findViewById(R.id.weather_icon);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

      //  Picasso builder = new Picasso.Builder(context).build();
        viewHolder.name.setText(weathers.get(position).getName());
        viewHolder.country.setText(weathers.get(position).getCountry());
        viewHolder.temp.setText(weathers.get(position).getTemp()+" °C");
        viewHolder.desc.setText(weathers.get(position).getDesc().toUpperCase());
        //viewHolder.icon.loadData("<img src='"+ weathers.get(position).getIcon() +"' />", "text/html; charset=utf-8", "utf-8");
        //viewHolder.icon.setVerticalScrollBarEnabled(false);

        String descId = weathers.get(position).getConditionId();

        if (descId.equals("800")) {
            viewHolder.icon2.setImageResource(R.mipmap.sunb);
        } else if (descId.equals("500") || descId.equals("300") || descId.equals("310") || descId.equals("311")) {
            viewHolder.icon2.setImageResource(R.mipmap.lightrain);
        } else if (descId.equals("520")) {
            viewHolder.icon2.setImageResource(R.mipmap.lightrain2);
        } else if (descId.equals("501") || descId.equals("521") || descId.equals("522")) {
            viewHolder.icon2.setImageResource(R.mipmap.showerrain);
        } else if (descId.equals("502") || descId.equals("503")) {
            viewHolder.icon2.setImageResource(R.mipmap.storm3);
        } else if (descId.equals("801") || descId.equals("803")) {
            viewHolder.icon2.setImageResource(R.mipmap.fewclouds);
        } else if (descId.equals("802")) {
            viewHolder.icon2.setImageResource(R.mipmap.scatteredclouds);
        } else if (descId.equals("804")) {
            viewHolder.icon2.setImageResource(R.mipmap.overcastcloud);
        } else if (descId.equals("600") || descId.equals("620")) {
            viewHolder.icon2.setImageResource(R.mipmap.snowy);
        } else if (descId.equals("601")) {
            viewHolder.icon2.setImageResource(R.mipmap.snowflake);
        } else if (descId.equals("602")) {
            viewHolder.icon2.setImageResource(R.mipmap.heavysnow);
        } else if (descId.equals("611") || descId.equals("612") || descId.equals("616")) {
            viewHolder.icon2.setImageResource(R.mipmap.sleet);
        } else if (descId.equals("615")) {
            viewHolder.icon2.setImageResource(R.mipmap.sleet2);
        } else if (descId.equals("211")) {
            viewHolder.icon2.setImageResource(R.mipmap.storm);
        } else if (descId.equals("200")) {
            viewHolder.icon2.setImageResource(R.mipmap.storm2);
        } else if (descId.equals("201") || descId.equals("202")) {
            viewHolder.icon2.setImageResource(R.mipmap.storm3);
        }  else if (descId.equals("711")) {
            viewHolder.icon2.setImageResource(R.mipmap.smoke);
        } else if (descId.equals("701") || descId.equals("721") || descId.equals("741")) {
            viewHolder.icon2.setImageResource(R.mipmap.fog);
        } else if (descId.equals("761")) {
            viewHolder.icon2.setImageResource(R.mipmap.dust);
        }


        return convertView;
    }
}
