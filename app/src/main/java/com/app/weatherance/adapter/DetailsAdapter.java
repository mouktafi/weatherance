package com.app.weatherance.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.weatherance.R;
import com.app.weatherance.model.Detail;

import java.util.ArrayList;

public class DetailsAdapter extends ArrayAdapter<Detail> {
    private ArrayList<Detail> details;
    private LayoutInflater inflater;
    private int Resource;
    Context context;

    public DetailsAdapter(@NonNull Context context, int resource, ArrayList<Detail> objects) {
        super(context, resource, objects);

        this.context = context;
        this.Resource = resource;
        this.details = objects;
        inflater = (LayoutInflater.from(context));
    }

    private class ViewHolder {
        TextView name;
       // SearchView citySearch;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(Resource, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView)convertView.findViewById(R.id.tv_cityname);
          //  viewHolder.citySearch = (SearchView)convertView.findViewById(R.id.sv_city);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.name.setText(details.get(position).getName());

        return convertView;
    }
}
