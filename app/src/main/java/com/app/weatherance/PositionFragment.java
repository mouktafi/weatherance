package com.app.weatherance;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Looper;
import android.util.FloatProperty;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.weatherance.adapter.DetailsAdapter;
import com.app.weatherance.model.Detail;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class PositionFragment extends Fragment {

    FusedLocationProviderClient client;
    int latInt,latInt1;
    int lonInt,lonInt1;
    String urlHttp;
    String APPID;
    String cityName;
    String cityCountry;
    String latStr;
    String lonStr;
    ArrayList<Detail> details;
    DetailsAdapter adapter;
    TextView tv_city_name;
    TextView tv_city_country;
    TextView tv_det_desc;
    TextView tv_det_temp;
    ImageView iv_detail_icon;
    int weatherDetailId;
    String weatherDetailDesc;
    String mainDetailTemp;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        client = LocationServices.getFusedLocationProviderClient(requireActivity());

        if (ContextCompat.checkSelfPermission(requireActivity()
                , Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(requireActivity()
                        , Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation();
            Log.i("sam", String.valueOf(lonInt));
        } else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
        }

        return inflater.inflate(R.layout.fragment_position, container, false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 100 && (grantResults.length > 0) && (grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
            getCurrentLocation();
            Log.i("sam", String.valueOf(lonInt));
        } else {
            Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        details = new ArrayList<Detail>();
        //ListView listView = findViewById(R.id.detail_list);
        tv_city_name = requireActivity().findViewById(R.id.tv_pos_cityname);
        tv_city_country= requireActivity().findViewById(R.id.tv_pos_country);
        tv_det_desc = requireActivity().findViewById(R.id.tv_pos_desc);
        iv_detail_icon = requireActivity().findViewById(R.id.iv_pos_icon);
        tv_det_temp = requireActivity().findViewById(R.id.tv_pos_temp);

        try {
            DownloadTask task = new DownloadTask();
            //http://api.openweathermap.org/data/2.5/forecast?q=Torino&units=metric&APPID=c26c578f70df09278c782b96e34e31ed
            APPID = "c26c578f70df09278c782b96e34e31ed";

            latStr = String.valueOf(latInt);
            lonStr = String.valueOf(lonInt);
            Log.i("latStr", latStr);
            urlHttp = "https://api.openweathermap.org/data/2.5/weather?lat="+latInt1+"&lon="+lonInt1+"&lang=it&units=metric&APPID="+ APPID;
            task.execute(urlHttp);

            adapter =  new DetailsAdapter(requireActivity(),R.layout.detail_item_weather, details);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getCurrentLocation() {

        LocationManager locationManager = (LocationManager) requireActivity().getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            client.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();

                    if (location != null) {
                        latInt = (int) location.getLatitude();
                        lonInt = (int) location.getLongitude();
                        Log.i("latitude", String.valueOf(latInt));
                        Log.i("longitude", String.valueOf(lonInt));
                    } else {
                        Log.i("boh", "boh");
                        LocationRequest locationRequest = new LocationRequest()
                                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                .setInterval(10000)
                                .setFastestInterval(1000)
                                .setNumUpdates(1);

                        LocationCallback locationCallback = new LocationCallback() {
                            @Override
                            public void onLocationResult(@NonNull LocationResult locationResult) {
                                Location location1 = locationResult.getLastLocation();
                                latInt1 = (int) location1.getLatitude();
                                lonInt1 = (int) location1.getLongitude();
                                Log.i("latitude1", String.valueOf(latInt1));
                                Log.i("longitude1", String.valueOf(lonInt1));

                            }
                        };
                        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        client.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                    }
                }
            });
        }else{
            Log.i("error", "error");
        }
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {

        private ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(requireActivity());
            dialog.setMessage("Loading, Please wait");
            dialog.setTitle("Connection to server");
            dialog.show();
            dialog.setCancelable(false);
        }


        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection;

            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();

                while (data != -1) {
                    char cur = (char)data;
                    result += cur;
                    data = reader.read();
                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            //Log.i("JSON", result);
            try {
                JSONObject jsonObject = new JSONObject(result);

                String weathers_str = jsonObject.getString("weather");
                String main_str = jsonObject.getString("main");
                String sys_str = jsonObject.getString("sys");

                Log.i("JSON_main", weathers_str);

                JSONArray array = new JSONArray(weathers_str);
                JSONObject mainObject = new JSONObject(main_str);
                JSONObject sysObject = new JSONObject(sys_str);

                Log.i("mainobject", String.valueOf(mainObject));

                Log.i("cityname", String.valueOf(array));



                //JSONObject jsonPartCity = arrayCity.getJSONObject(i);

                //JSONArray weatherArray = jsonPart.getJSONArray("weather");
                //JSONObject mainObject = jsonPart.getJSONObject("main");

                JSONObject weather0Object  = array.getJSONObject(0);
                //JSONObject cityObject = jsonPartCity.getJSONObject("city");


                //String cityTemp = mainObject.getString("temp");
                //String cityName = arrayCity.getString("name");
                cityCountry = sysObject.getString("country");
                weatherDetailId = weather0Object.getInt("id");
                weatherDetailDesc = weather0Object.getString("description");
                mainDetailTemp = mainObject.getString("temp");

                    /*JSONArray weatherDetObject = jsonPart.getJSONArray("temp");
                    JSONObject weatherDet0Object  = weatherDetObject.getJSONObject(0);
                    JSONObject cloudsObject = jsonPart.getJSONObject("clouds");
                    JSONObject sysObject = jsonPart.getJSONObject("sys");

                    String cityName = jsonPart.getString("name");
                    String cityCountry = sysObject.getString("country");
                    String cityTemp = weatherObject.getString("temp");
                    String cityDescId = weatherDet0Object.getString("id");
                    String cityDesc = weatherDet0Object.getString("description");
                    String cityIcon = weatherDet0Object.getString("icon");
                    String cityPress = weatherObject.getString("pressure");
                    String cityClouds = cloudsObject.getString("all");
                    String cityIconUrl = "http://openweathermap.org/img/wn/"+ cityIcon + "@2x.png";*/


                // wea.setTemp(cityTemp);

                //tv_det_cityname.setText(extra);
                tv_city_name.setText(cityName);
                tv_city_country.setText(cityCountry);
                tv_det_desc.setText(weatherDetailDesc);
                tv_det_temp.setText(mainDetailTemp);
                if (weatherDetailId >= 200 && weatherDetailId <= 232) {
                    iv_detail_icon.setImageResource(R.mipmap.storm3);
                }else if (weatherDetailId >= 300 && weatherDetailId <= 321){
                    iv_detail_icon.setImageResource(R.mipmap.lightrain);
                }else if (weatherDetailId >= 500 && weatherDetailId <= 504){
                    iv_detail_icon.setImageResource(R.mipmap.lightrain);
                }else if (weatherDetailId >= 511 && weatherDetailId <= 531){
                    iv_detail_icon.setImageResource(R.mipmap.showerrain);
                }else if (weatherDetailId >= 600 && weatherDetailId <= 622){
                    iv_detail_icon.setImageResource(R.mipmap.snowflake);
                }else if (weatherDetailId == 701 || weatherDetailId == 721 || weatherDetailId == 741){
                    iv_detail_icon.setImageResource(R.mipmap.fog);
                }else if (weatherDetailId == 711 || weatherDetailId == 762){
                    iv_detail_icon.setImageResource(R.mipmap.smoke);
                }else if (weatherDetailId == 761 || weatherDetailId == 731 || weatherDetailId == 751 || weatherDetailId == 771 || weatherDetailId == 781){
                    iv_detail_icon.setImageResource(R.mipmap.dust);
                }else if (weatherDetailId == 800){
                    iv_detail_icon.setImageResource(R.mipmap.sunb);
                }else if (weatherDetailId == 801){
                    iv_detail_icon.setImageResource(R.mipmap.fewclouds);
                }else if (weatherDetailId == 802){
                    iv_detail_icon.setImageResource(R.mipmap.scatteredclouds);
                }else if (weatherDetailId == 803){
                    iv_detail_icon.setImageResource(R.mipmap.fewclouds);
                }else if (weatherDetailId == 804){
                    iv_detail_icon.setImageResource(R.mipmap.overcastcloud);
                }

                Log.i("JSON2", String.valueOf(mainDetailTemp));
                Log.i("JSON_country", cityCountry);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            dialog.cancel();
            adapter.notifyDataSetChanged();
        }
    }
}