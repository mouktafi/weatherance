package com.app.weatherance.model;

public class Detail {

    private long id;
    private String name;
    private String temp;

    public  Detail() {
/*
        this.name = name;
        this.temp = temp;
        this.press = press;
        this.clouds = clouds;
*/

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

}
