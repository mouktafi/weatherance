package com.app.weatherance;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.app.weatherance.adapter.WeathersAdapter;
import com.app.weatherance.model.Weather;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;

    Fragment fr;
    FragmentManager fm;
    FragmentTransaction tx;

/*    ArrayList<Weather> weathers;
    WeathersAdapter adapter;
    ListView listView;
    SearchView citySearch;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.my_drawer_layout);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.nav_open, R.string.nav_close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        injectFragment();

  /*      weathers = new ArrayList<Weather>();
        listView = findViewById(R.id.weather_list);
        citySearch = findViewById(R.id.sv_city);*/

        navigationView = (NavigationView) findViewById(R.id.nv_navigation_view);
        navigationView.setNavigationItemSelectedListener(item -> {
            drawerLayout.closeDrawer(GravityCompat.START);

            switch (item.getItemId()) {
                case R.id.nav_home:
                    //Log.i("nav", "home: ");
                    fr = new PositionFragment();
                    changeFragment();
                    break;
                case R.id.nav_settings:
                    //Log.i("nav", "settings: ");
                    fr = new ListFragment();
                    changeFragment();
                    break;
            }

            return true;
        });

        /*DownloadTask task = new DownloadTask();
            String APPID = "c26c578f70df09278c782b96e34e31ed";
        task.execute("http://api.openweathermap.org/data/2.5/group?id=3165525,3166596,3178479,5128581,6094817,2643741,524901,1850147,2158177,3369157,292223,293397,1735161,3448439,1275339,1795565,3530597,3882428,4164138,1871859&lang=it&units=metric&APPID="+ APPID);

        adapter =  new WeathersAdapter(getApplicationContext(),R.layout.item_weather, weathers);
        listView.setAdapter(adapter);

        *//*listView.setOnItemClickListener((adapter, view, position, arg) -> {
            Intent goDetailAct = new Intent(MainActivity.this, DetailActivity.class);
            goDetailAct.putExtra("cityName","ottawa");
            startActivity(goDetailAct);
        });*//*
        citySearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Intent i = new Intent(MainActivity.this, DetailActivity.class);
                i.putExtra("cityName", s);
                startActivity(i);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });*/
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigation_menu,menu);
        return true;
    }

    public void injectFragment() {
        fm = getSupportFragmentManager();
        tx = fm.beginTransaction();
        //aggiungere fragmentHome...
        PositionFragment positionFragment = new PositionFragment();
        tx.add(R.id.fragment_position, positionFragment);
        tx.commit();
    }

    public void changeFragment() {
        fm = getSupportFragmentManager();
        tx = fm.beginTransaction();
        tx.replace(R.id.fragment_position,fr);
        tx.commit();
    }


/*    public class DownloadTask extends AsyncTask<String, Void, String>{

        private ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(MainActivity.this);
            dialog.setMessage("Loading, Please wait");
            dialog.setTitle("Connection to server");
            dialog.show();
            dialog.setCancelable(false);
        }


        @Override
        protected String doInBackground(String... strings) {

            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();

                while (data != -1) {
                    char cur = (char)data;
                    result += cur;
                    data = reader.read();
                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
           // Log.i("JSON", result);

            try {
                JSONObject jsonObject = new JSONObject(result);

                String weathers_str = jsonObject.getString("list");

                //Log.i("JSON", weathers_str);

                JSONArray array = new JSONArray(weathers_str);

                for (int i=0; i<array.length(); i++){

                    Weather wea = new Weather();

                    JSONObject jsonPart = array.getJSONObject(i);
                    JSONObject weatherObject = jsonPart.getJSONObject("main");
                    JSONArray weatherDetObject = jsonPart.getJSONArray("weather");
                    JSONObject weatherDet0Object  = weatherDetObject.getJSONObject(0);
                    JSONObject cloudsObject = jsonPart.getJSONObject("clouds");
                    JSONObject sysObject = jsonPart.getJSONObject("sys");

                    String cityName = jsonPart.getString("name");
                    String cityCountry = sysObject.getString("country");
                    String cityTemp = weatherObject.getString("temp");
                    String cityDescId = weatherDet0Object.getString("id");
                    String cityDesc = weatherDet0Object.getString("description");
                   // String cityIcon = weatherDet0Object.getString("icon");
                    String cityPress = weatherObject.getString("pressure");
                    String cityClouds = cloudsObject.getString("all");
                    //String cityIconUrl = "http://openweathermap.org/img/wn/"+ cityIcon + "@2x.png";


                    wea.setName(cityName);
                    wea.setCountry(cityCountry);
                    wea.setTemp(cityTemp);
                    wea.setConditionId(cityDescId);
                    wea.setDesc(cityDesc);
                    //wea.setIcon(cityIconUrl);
                    wea.setPress(cityPress);
                    wea.setClouds(cityClouds);

                    weathers.add(wea);
                   // Log.i("JSON", cityName + " " + cityTemp + " " + cityClouds + " " + cityPress + " " + cityCountry + " " + cityDescId + " " + cityIconUrl);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        JSONObject cities;
                        String city;
                        JSONObject countries;
                        String country;
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            try {
                                cities = array.getJSONObject(position);
                                city = cities.getString("name");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.i("toastred", city);
                            Intent i = new Intent(MainActivity.this,DetailActivity.class);
                            i.putExtra("cityName",city);
                            startActivity(i);
                        }
                    });
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialog.cancel();
            adapter.notifyDataSetChanged();
        }
    }*/
}