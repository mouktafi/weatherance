package com.app.weatherance;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.app.weatherance.adapter.DetailsAdapter;
import com.app.weatherance.model.Detail;
import com.google.android.material.appbar.AppBarLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    Bundle b;
    String cityName;
    String cityCountry;
    ArrayList<Detail> details;
    DetailsAdapter adapter;
    TextView tv_city_name;
    TextView tv_city_country;
    TextView tv_det_desc;
    TextView tv_det_temp;
    ImageView iv_detail_icon;
    int weatherDetailId;
    String weatherDetailDesc;
    String mainDetailTemp;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        b = getIntent().getExtras();
        cityName = b.getString("cityName");
        //cityCountry = b.getString("cityCountry");
        Toast.makeText(this,cityName,Toast.LENGTH_SHORT).show();
        setTitle(cityName);
        details = new ArrayList<Detail>();
        //ListView listView = findViewById(R.id.detail_list);
        tv_city_name = findViewById(R.id.tv_det_cityname);
        tv_city_country= findViewById(R.id.tv_det_country);
        tv_det_desc = findViewById(R.id.tv_detail_desc);
        iv_detail_icon = findViewById(R.id.iv_detail_icon);
        tv_det_temp = findViewById(R.id.tv_detail_temp);

        try {
            DetailActivity.DownloadTask task = new DetailActivity.DownloadTask();
            //http://api.openweathermap.org/data/2.5/forecast?q=Torino&units=metric&APPID=c26c578f70df09278c782b96e34e31ed
            String APPID = "c26c578f70df09278c782b96e34e31ed";
            task.execute("https://api.openweathermap.org/data/2.5/weather?q="+cityName+"&lang=it&units=metric&APPID="+ APPID);

            adapter =  new DetailsAdapter(getApplicationContext(),R.layout.detail_item_weather, details);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //listView.setAdapter(adapter);

    }
    public class DownloadTask extends AsyncTask<String, Void, String> {

        private ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(DetailActivity.this);
            dialog.setMessage("Loading, Please wait");
            dialog.setTitle("Connection to server");
            dialog.show();
            dialog.setCancelable(false);
        }


        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection;

            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();

                while (data != -1) {
                    char cur = (char)data;
                    result += cur;
                    data = reader.read();
                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            //Log.i("JSON", result);
            try {
                JSONObject jsonObject = new JSONObject(result);

                String weathers_str = jsonObject.getString("weather");
                String main_str = jsonObject.getString("main");
                String sys_str = jsonObject.getString("sys");

                Log.i("JSON_main", sys_str);

                JSONArray array = new JSONArray(weathers_str);
                JSONObject mainObject = new JSONObject(main_str);
                JSONObject sysObject = new JSONObject(sys_str);

                Log.i("cityname", String.valueOf(array));



                    //JSONObject jsonPartCity = arrayCity.getJSONObject(i);

                    //JSONArray weatherArray = jsonPart.getJSONArray("weather");
                    //JSONObject mainObject = jsonPart.getJSONObject("main");

                    JSONObject weather0Object  = array.getJSONObject(0);
                    //JSONObject cityObject = jsonPartCity.getJSONObject("city");


                    //String cityTemp = mainObject.getString("temp");
                    //String cityName = arrayCity.getString("name");
                    cityCountry = sysObject.getString("country");
                    weatherDetailId = weather0Object.getInt("id");
                    weatherDetailDesc = weather0Object.getString("description");
                    mainDetailTemp = mainObject.getString("temp");
                    /*JSONArray weatherDetObject = jsonPart.getJSONArray("temp");
                    JSONObject weatherDet0Object  = weatherDetObject.getJSONObject(0);
                    JSONObject cloudsObject = jsonPart.getJSONObject("clouds");
                    JSONObject sysObject = jsonPart.getJSONObject("sys");

                    String cityName = jsonPart.getString("name");
                    String cityCountry = sysObject.getString("country");
                    String cityTemp = weatherObject.getString("temp");
                    String cityDescId = weatherDet0Object.getString("id");
                    String cityDesc = weatherDet0Object.getString("description");
                    String cityIcon = weatherDet0Object.getString("icon");
                    String cityPress = weatherObject.getString("pressure");
                    String cityClouds = cloudsObject.getString("all");
                    String cityIconUrl = "http://openweathermap.org/img/wn/"+ cityIcon + "@2x.png";*/


                   // wea.setTemp(cityTemp);

                    //tv_det_cityname.setText(extra);
                    tv_city_name.setText(cityName);
                    tv_city_country.setText(cityCountry);
                    tv_det_desc.setText(weatherDetailDesc);
                    tv_det_temp.setText(mainDetailTemp);

                    if (weatherDetailId >= 200 && weatherDetailId <= 232) {
                        iv_detail_icon.setImageResource(R.mipmap.storm3);
                    }else if (weatherDetailId >= 300 && weatherDetailId <= 321){
                        iv_detail_icon.setImageResource(R.mipmap.lightrain);
                    }else if (weatherDetailId >= 500 && weatherDetailId <= 504){
                        iv_detail_icon.setImageResource(R.mipmap.lightrain);
                    }else if (weatherDetailId >= 511 && weatherDetailId <= 531){
                        iv_detail_icon.setImageResource(R.mipmap.showerrain);
                    }else if (weatherDetailId >= 600 && weatherDetailId <= 622){
                        iv_detail_icon.setImageResource(R.mipmap.snowflake);
                    }else if (weatherDetailId == 701 || weatherDetailId == 721 || weatherDetailId == 741){
                        iv_detail_icon.setImageResource(R.mipmap.fog);
                    }else if (weatherDetailId == 711 || weatherDetailId == 762){
                        iv_detail_icon.setImageResource(R.mipmap.smoke);
                    }else if (weatherDetailId == 761 || weatherDetailId == 731 || weatherDetailId == 751 || weatherDetailId == 771 || weatherDetailId == 781){
                        iv_detail_icon.setImageResource(R.mipmap.dust);
                    }else if (weatherDetailId == 800){
                        iv_detail_icon.setImageResource(R.mipmap.sunb);
                    }else if (weatherDetailId == 801){
                        iv_detail_icon.setImageResource(R.mipmap.fewclouds);
                    }else if (weatherDetailId == 802){
                        iv_detail_icon.setImageResource(R.mipmap.scatteredclouds);
                    }else if (weatherDetailId == 803){
                        iv_detail_icon.setImageResource(R.mipmap.fewclouds);
                    }else if (weatherDetailId == 804){
                        iv_detail_icon.setImageResource(R.mipmap.overcastcloud);
                    }

                    Log.i("JSON2", String.valueOf(mainDetailTemp));
                    Log.i("JSON_country", cityCountry);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            dialog.cancel();
            adapter.notifyDataSetChanged();
        }
    }
}